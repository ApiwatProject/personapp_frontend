import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:personapp/models/person.dart';
import 'package:personapp/models/response_header.dart';
import 'package:personapp/repositories/person_repository.dart';

part 'person_event.dart';
part 'person_state.dart';

class PersonBloc extends Bloc<PersonEvent, PersonState> {
  PersonBloc() : super(PersonInitial());

  @override
  Stream<PersonState> mapEventToState(
    PersonEvent event,
  ) async* {
    if (event is PersonAdd) {
      yield* _mapPersonAddPersonToState(event);
    } else if (event is PersonFetch){
      yield* _mapPersonFetchToState(event);
    } else if (event is PersonPrepare){
      yield PersonAddPrepare();
    }
  }
  @override
  Future<void> close() {
    return super.close();
  }
  Stream<PersonState> _mapPersonAddPersonToState(
      PersonAdd personAddPerson) async* {
    yield PersonAddLoading();
    PersonRepository personRepository = new PersonRepository();
    try {
      var response = await personRepository.addPerson(person: personAddPerson.person);
      personRepository.errorResponse(response);
      yield PersonAddSuccess();
    } on Exception catch (ex) {
      yield PersonAddFailed(statusCodeError: ex.toString());
    } 
  }
  Stream<PersonState> _mapPersonFetchToState(
      PersonFetch personAddPerson) async* {
    yield PersonGetLoading();
    await Future.delayed(Duration(seconds: 3));
    PersonRepository personRepository = new PersonRepository();
    try {
      var response = await personRepository.getAllPerson();
      personRepository.errorResponse(response);
      var responseHeader = ResponseHeaderModel.fromJson(response.body);
      Iterable<dynamic> contentList = responseHeader.content as List; 
      List<Person> personList = contentList.map((element) => Person.fromMap(element)).toList();
      yield PersonGetSuccess(personList:personList);
    } on Exception catch (ex) {
      yield PersonGetFailed(statusCodeError: ex.toString());
    } 
  }
}
