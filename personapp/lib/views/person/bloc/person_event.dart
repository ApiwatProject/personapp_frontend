part of 'person_bloc.dart';

abstract class PersonEvent extends Equatable {
  const PersonEvent();

  @override
  List<Object> get props => [];
}

class PersonAdd extends PersonEvent {
  final Person person;

  const PersonAdd({
    @required this.person,
  }) : super();
}

class PersonFetch extends PersonEvent {
}

class PersonPrepare extends PersonEvent {
}
