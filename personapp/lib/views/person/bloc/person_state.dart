part of 'person_bloc.dart';

abstract class PersonState extends Equatable {
  const PersonState();

  @override
  List<Object> get props => [];
}

class PersonInitial extends PersonState {}
class PersonAddPrepare extends PersonState {}
class PersonAddLoading extends PersonState {}
class PersonGetLoading extends PersonState {}
class PersonAddSuccess extends PersonState {}
class PersonGetSuccess extends PersonState {
  final List<Person> personList;
  PersonGetSuccess({ @required  this.personList});
}

class PersonAddFailed extends PersonState {
  final String statusCodeError;
  PersonAddFailed({
    @required this.statusCodeError,
  });
}
class PersonGetFailed extends PersonState {
  final String statusCodeError;
  PersonGetFailed({
    @required this.statusCodeError,
  });
}
