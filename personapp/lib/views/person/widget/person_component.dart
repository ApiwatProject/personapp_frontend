import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:personapp/views/person/bloc/person_bloc.dart';
import 'package:personapp/views/person/widget/person_listview.dart';

class PersonComponent extends StatefulWidget {
  @override
  _PersonComponentState createState() => _PersonComponentState();
}

class _PersonComponentState extends State<PersonComponent> {
  @override
  void initState() {
    BlocProvider.of<PersonBloc>(context).add(PersonFetch());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<PersonBloc, PersonState>(
      listener: (context, state) async {
        if (state is PersonAddSuccess) {
          Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Text('Add person Successful'),
            ),
          );
          BlocProvider.of<PersonBloc>(context).add(PersonFetch());
        } else if (state is PersonAddFailed) {
          Scaffold.of(context).showSnackBar(
            SnackBar(
              backgroundColor: Colors.redAccent,
              content: Text(
                'Add person Failed ${state.statusCodeError}',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
          );
        } else if (state is PersonAddLoading){
          Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Text('Adding person to system'),
            ),
          );
        }
      },
      buildWhen: (previous, current) {
        return current is PersonGetLoading ||
            current is PersonGetSuccess ||
            current is PersonGetFailed;
      },
      builder: (BuildContext context, state) {
        if (state is PersonGetLoading) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else if (state is PersonGetSuccess) {
          if (state.personList.length == 0) {
            return Center(
              child: Text('no person right now'),
            );
          } else {
            return PersonListView(
              personList: state.personList,
            );
          }
        } else if (state is PersonGetFailed) {
          return Center(child: Text("error  ${state.statusCodeError}"));
        }
        return Center(child: Text("error unknown state  ${state}"));
      },
    );
  }
}
