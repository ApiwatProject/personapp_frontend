import 'package:flutter/material.dart';
import 'package:personapp/models/person.dart';

class PersonListView extends StatelessWidget {
  final List<Person> personList;

  const PersonListView({@required this.personList});
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: personList.length,
      itemBuilder: (BuildContext context, int index) {
        return Card(
          child: ListTile(
            leading: Icon(Icons.account_circle),
            title: Text(
                '${personList[index].userName} ${personList[index].userSurname}'),
          ),
        );
      },
    );
  }
}
