import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:personapp/views/person/bloc/person_bloc.dart';
import 'package:personapp/views/person/widget/person_component.dart';
import 'package:personapp/widget/add_person_dialog.dart';

class PersonScreen extends StatefulWidget {
  @override
  _PersonScreenState createState() => _PersonScreenState();
}

class _PersonScreenState extends State<PersonScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Person List'),
      ),
      body: PersonComponent(),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          var result = await formDialog(context);
          BlocProvider.of<PersonBloc>(context).add(PersonAdd(person: result));
        },
        child: Icon(Icons.add),
      ),
    );
  }
}
