import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:personapp/models/person.dart';
import 'package:personapp/views/person/bloc/person_bloc.dart';
import 'package:personapp/widget/border_style.dart';

Future<Person> formDialog(BuildContext context) async {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _name = TextEditingController();
  TextEditingController _surname = TextEditingController();

  return showDialog<Person>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Add Person'),
        content: Container(
          height: 200,
          child: Form(
            key: _formKey,
            child: Column(children: <Widget>[
              TextFormField(
                controller: _name,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter some name';
                  }
                  return null;
                },
                decoration: InputDecoration(
                  labelText: 'UserName',
                  enabledBorder: outlineTemplate(Colors.black),
                  focusedBorder:outlineTemplate(Colors.black),
                  errorBorder: outlineTemplate(Colors.redAccent),
                  focusedErrorBorder: outlineTemplate(Colors.redAccent),
                ),
              ),
              SizedBox(height:10),
              TextFormField(
                controller: _surname,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter some surname';
                  }
                  return null;
                },
                decoration: InputDecoration(
                  labelText: 'UserSurname',
                  enabledBorder: outlineTemplate(Colors.black),
                  focusedBorder:outlineTemplate(Colors.black),
                  errorBorder: outlineTemplate(Colors.redAccent),
                  focusedErrorBorder: outlineTemplate(Colors.redAccent),
                ),
              ),
            ]),
          ),
        ),
        actions: <Widget>[
          TextButton(
            child: Text('Add'),
            onPressed: () {
              if (_formKey.currentState.validate()) {
                Person person = new Person(userName: _name.text,userSurname: _surname.text );
                Navigator.of(context).pop(person);
              }
            },
          ),
        ],
      );
    },
  );
}
