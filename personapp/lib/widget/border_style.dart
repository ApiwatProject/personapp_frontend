import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

OutlineInputBorder outlineTemplate(Color colors) {
  return OutlineInputBorder(
    borderSide: BorderSide(color: colors, width: 1.0),
  );
}
