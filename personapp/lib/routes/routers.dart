import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:personapp/views/person/bloc/person_bloc.dart';
import 'package:personapp/views/person/screen/person_screen.dart';

class Routers {
  final PersonBloc _personBloc = PersonBloc();

  Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(
            builder: (_) => BlocProvider(
                create: (BuildContext context) => _personBloc,
                child: PersonScreen()));
        break;
      default:
        return MaterialPageRoute(
          builder: (context) => Scaffold(
            body: Center(
              child: Text('No path for ${settings.name}'),
            ),
          ),
        );
        break;
    }
  }

  void dispose(){
    _personBloc.close();
  }
}