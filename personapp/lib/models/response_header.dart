import 'dart:convert';

class ResponseHeaderModel {
  String status;
  String message;
  dynamic content;
  
  ResponseHeaderModel({
    this.status,
    this.message,
    this.content,
  });

  Map<String, dynamic> toMap() {
    return {
      'status': status,
      'message': message,
      'content': content,
    };
  }

  factory ResponseHeaderModel.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return ResponseHeaderModel(
      status: map['status'],
      message: map['message'],
      content: map['content'],
    );
  }

  String toJson() => json.encode(toMap());

  factory ResponseHeaderModel.fromJson(String source) => ResponseHeaderModel.fromMap(json.decode(source));
}
