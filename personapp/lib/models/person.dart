import 'dart:convert';

class Person {
  String userName;
  String userSurname;
  Person({
    this.userName,
    this.userSurname,
  });
  

  Map<String, dynamic> toMap() {
    return {
      'userName': userName,
      'userSurname': userSurname,
    };
  }

  factory Person.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return Person(
      userName: map['userName'],
      userSurname: map['userSurname'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Person.fromJson(String source) => Person.fromMap(json.decode(source));
}
