import 'package:flutter/material.dart';
import 'package:personapp/routes/routers.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final Routers _routers = Routers();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: "/",
      onGenerateRoute:_routers.generateRoute ,
    );
  }
}
