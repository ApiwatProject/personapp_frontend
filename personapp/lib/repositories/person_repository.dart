import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:personapp/models/person.dart';
import 'package:personapp/repositories/base_repository.dart';

class PersonRepository extends BaseRepository {
  Future<http.Response> getAllPerson() async {
    String url = setPath("person");
    return await http.get(url, headers: headers);
  }

  Future<http.Response> addPerson({@required Person person}) async {
    String url = setPath("person");
    return await http.post(url, headers: headers, body: person.toJson());
  }
}
