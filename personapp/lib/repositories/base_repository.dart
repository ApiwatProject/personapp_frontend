import 'package:http/http.dart';
import 'package:personapp/utils/exception.dart';

abstract class BaseRepository {
  String baseUrl = 'http://139.59.229.66:5001/api/';
  Map<String, String> headers = {
    "Content-type": "application/json; charset=utf-8 ",
    "Authorization": "Bearer token"
  };

  String setPath(String path){
    return baseUrl + path;
  }

  void errorResponse(Response response){
    if (response.statusCode == 401) {
      throw UnauthorisedException(401);
    } else if(response.statusCode == 400){
      throw BadRequestException(400);
    } else if(response.statusCode == 500){
      throw InternalServerException(500);
    }
  }
}